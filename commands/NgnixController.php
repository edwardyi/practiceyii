<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Ngnix Service Starter
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class NgnixController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($message = 'ngnix console starting....')
    {
        echo $message . "\n";

        $version = shell_exec("php -v");
        // 路徑也寫反斜線
        $change_dir = shell_exec("D:/wamp/apps/nginx/nginx.exe");

        return ExitCode::OK;
    }

    public function actionStartphp()
    {
      echo "php starting...";
      $start = shell_exec("D:/wamp/bin/php/php7.1.22/php-cgi.exe -b 127.0.0.1:8888");
      
      return ExitCode::OK;
    }
}
