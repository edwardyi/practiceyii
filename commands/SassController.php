<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Sass Compiler
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class SassController extends Controller
{
    /**
     * Sass Compiler
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($message = 'sass index')
    {
        echo $message . "\n";
        // $this->stdout("AAA?\n", Console::BOLD);

        return ExitCode::OK;
    }
}
