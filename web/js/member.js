reloadMemberTable();

if (window.location.pathname.indexOf('user/update') > 0) {
	getUpdateMember()
}

function removeLabel(ele, target="label")
{
	if (ele.next().prop("tagName")==target.toUpperCase()) {
		ele.next().remove();
	}
}

function createMember()
{
	let isValidate = false;
	let name = $("#name");
	let account = $('#account');
	let password = $('#password');
	let email = $('#email');
	let memberForm = $("#memberForm");
 
	removeLabel(name);
	removeLabel(account);
	removeLabel(password);
	removeLabel(email);

	if (account.val()=="") {
		$(account).after(`<label style="color:red;">請設定帳號!!</label>`);
	}

	if (password.val()=="") {
		$(password).after(`<label style="color:red;">請設定密碼!!</label>`);
	}

	if (email.val()=="") {
		$(email).after(`<label style="color:red;">請設定Email!!</label>`);
	}

	if (account.val() && password.val() && email.val()) {
		isValidate=true;
	}
	let formData = memberForm.serialize();
	// console.log(data);
	// console.log(account,password,email)
	if(isValidate) {
		$.ajax({
			url: "../rest/user/create", 
			type: "post",
			dataType: 'json',
			data: formData,
			success: function(data){
				console.log(data,formData)
				if (data.code != 200) {
					alert(data.message);
					return false;
				}

				console.log("新增成功!!")
				alert("新增成功!")
				location.href="index";
				return true;
			}});
	}
}


function reloadMemberTable() {
	$("#member").find('tbody').remove();
	$("#member").append('<tbody></tbody>');
	$.ajax({
		url: "../rest/user/index", 
		type: "get",
		dataType: 'json',
		success: function(data){
			console.log(data)
			if(data.code == 200) {
				$.each(data.data ,function(key, value) {
					console.log(value)
					$('#member').append(`<tr>
											<td>${value.account}</td>
											<td>${value.name}</td>
											<td>${value.email}</td>
											<td>${value.created_at}</td>
											<td>${value.updated_at}</td>
											<td>
												<a class="btn btn-info" href="update?id=${value.id}">修改</a>
												<a class="btn btn-danger" onclick="deleteMember(${value.id})">刪除</a>
											</td>
										</tr>`);
					// console.log(key,value)
				})
				// console.log(data.data)
			}
		}});
}
function getUpdateMember(){
	let isValidate = false;
	let name = $("#name");
	let account = $('#account');
	let password = $('#password');
	let email = $('#email');
	let memberForm = $("#memberForm");
	let id = $('#id');
	let url = window.location.search;
	var match = url.match(/id=(\d+)/)
	var mid = match[1];
	$.ajax({
		url: '../rest/user/view?id='+mid,
		dataType: 'json',
		type: 'get',
		success: function(data){
			console.log(data.data);
			let row = data.data;
			if(data.code == 200) {
				name.val(row.name)
				account.val(row.account)
				email.val(row.email)
				password.val(row.password)
				id.val(row.id)
			}
		}});
}

function updateMemeber($mId)
{
	let isValidate = false;
	let name = $("#name");
	let account = $('#account');
	let password = $('#password');
	let email = $('#email');
	let memberForm = $("#memberForm");
 
	removeLabel(name);
	removeLabel(account);
	removeLabel(password);
	removeLabel(email);

	if (account.val()=="") {
		$(account).after(`<label style="color:red;">請設定帳號!!</label>`);
	}

	if (password.val()=="") {
		$(password).after(`<label style="color:red;">請設定密碼!!</label>`);
	}

	if (email.val()=="") {
		$(email).after(`<label style="color:red;">請設定Email!!</label>`);
	}

	if (account.val() && password.val() && email.val()) {
		isValidate=true;
	}
	let formData = memberForm.serialize();
	// console.log(data);
	// console.log(account,password,email)
	if(isValidate) {
		$.ajax({
			url: "../rest/user/update", 
			type: "post",
			dataType: 'json',
			data: formData,
			success: function(data){
				console.log(data,formData)
				if (data.code != 200) {
					alert(data);
					return false;
				}
				alert("修改成功!")
				location.href="/user/index";
				return true;
			}});
	}
	
}


function deleteMember($mId)
{
	let isDelete = confirm("確認要刪除?");
	if (isDelete) {
		console.log('deleting');
			$.ajax({
				url: "../rest/user/delete?id="+$mId, 
				dataType: 'json',
				type: 'delete',
				success: function(data){
					if (data.code != 200) {
						console.log(data);
						alert(data.message);
						return false;
					}
					console.log('reloading', data)
					reloadMemberTable();
					alert('刪除成功!');
					return true;
				}});
	}
	
}