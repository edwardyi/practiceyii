<?php
use yii\web\UrlRule;
return array(
	[

			'class' => 'yii\rest\UrlRule',

			'controller' => 'api/user',

	],
  // array('user/rest/index',  'pattern' => 'api/%user/rest%', 'verb' => 'GET', 'parsingOnly' => true),
	// REST routes for CRUD operations
	'POST <controller:\w+>s' => '<controller>/create', // 'mode' => UrlRule::PARSING_ONLY will be implicit here
	'<controller:\w+>s'      => '<controller>/index',
	'PUT <controller:\w+>/<id:\d+>'    => '<controller>/update', // 'mode' => UrlRule::PARSING_ONLY will be implicit here
	'DELETE <controller:\w+>/<id:\d+>' => '<controller>/delete', // 'mode' => UrlRule::PARSING_ONLY will be implicit here
	'<controller:\w+>/<id:\d+>'        => '<controller>/view',
	// normal routes for CRUD operations
	'<controller:\w+>s/create' => '<controller>/create',
	'<controller:\w+>/<id:\d+>/<action:update|delete>' => '<controller>/<action>',
);