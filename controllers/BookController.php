<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

use app\models\Book\Books;
use app\models\Book\BookForm;
use app\models\Book\BookSearch;
use app\models\UploadForm;

use app\models\Category\BookCategory;

use yii\web\UploadedFile;

use app\components\TraitComponent;

class BookController extends Controller
{
  public function actionIndex()
  {
    $books = new BookSearch();
    // 用url參數來查詢資料
    $dataProvider = $books->search(Yii::$app->request->queryParams);
    Yii::info('開啟Borrow Index'.json_encode($dataProvider));
    return $this->render('index', [
        'book' => $books,
        'dataProvider' => $dataProvider,
    ]);
  }

  public function actionCreate()
  {
    $model = new Books();
    
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      Yii::$app->session->setFlash('success', "新增成功");
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('create', ['model'=> $model]);
    }
  }

  public function actionBorrow()
  {
    $model = new Books();
    
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      $uploadModel = new UploadForm();
      if ($uploadModel->upload()) {
          // 文件上传成功
          return $this->redirect(['view', 'id' => $model->id]);
      }
    } else {
      return $this->render('borrow', ['model'=> $model]);
    }
  }

  public function actionUpdate($id)
  {
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      $uploadModel = new UploadForm();
      // var_dump(UploadedFile::getInstances($model, 'imageFile'));
      // 從model取得檔案上傳的資料設定到屬性
      $uploadModel->imageFile = UploadedFile::getInstances($model, 'imageFile');
      if ($uploadModel->upload()) {
          // 文件上传成功
          return $this->redirect(['view', 'id' => $model->id]);
      }
      Yii::$app->session->setFlash('success', "更新成功");
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('update', ['model'=> $model]);
    }
  }

  public function actionDelete($id)
  {
    $this->findModel($id)->delete();
    Yii::$app->session->setFlash('success', "刪除成功");
    return $this->redirect(['index']);
  }

  public function actionView($id)
  {
    return $this->render('view',[
      'model' => $this->findModel($id),
    ]);
  }

  public function findModel($id)
  {
    if( ($model = Books::findOne($id)) != null) {
      return $model;
    } else {
      throw new NoFoundHttpException("找不到請求的頁面");
    }
  }

  // 暫不使用
  public function actionNewCategory()
  {
    $model = new BookCategory();
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('newcategory', ['model'=> $model]);
    }
  }
}