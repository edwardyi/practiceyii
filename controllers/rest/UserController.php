<?php
namespace app\controllers\rest;

use Yii;
use app\models\Members;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
use \yii\helpers\ArrayHelper;
use app\helpers\MemberHelper;

// 預先定義Exception
use \Exception as Exception;

/**
* UserController implements the CRUD actions for User model.
*/
class UserController extends Controller
{

	public function behaviors()
	{
		return [
			'verbs' => [
			'class' => VerbFilter::className(),
			'actions' => [
				'index'=>['get'],
				'view'=>['get'],
				'create'=>['post'],
				'update'=>['post'],
				'delete'=>['delete']
			],
		  ]
	  ];
	}
		
  
	public function beforeAction($event)
	{
		$action = $event->id;
		// 判斷是否有設定行為
		if (isset($this->actions[$action])) {
			$verbs = $this->actions[$action];
		} elseif (isset($this->actions['*'])) {
			$verbs = $this->actions['*'];
		} else {
			return $event->isValid;
		}
		// 取得當前的動作
		$verb = Yii::$app->getRequest()->getMethod();
			
		$allowed = array_map('strtoupper', $verbs);
		  
		if (!in_array($verb, $allowed)) {
			$result = MemberHelper::getErrorResponse(400, sprint('不允許%s方法', $action));
			return $this->asJson($result);
		}

		return true;  
	}
  
	public function actionIndex()
	{
	  try {
		$filter=array();
		$sort="";
		
		$page = (int) ArrayHelper::getValue($_GET, 'page' , 1);
		$limit = (int) ArrayHelper::getValue($_GET, 'limit' , 10);
			
		$offset=$limit*($page-1);
		$query=new Query;
		$query->offset($offset)
			->limit($limit)
			->from('members');
		
		// echo $query->createCommand()->rawSql;exit;
			
		$command = $query->createCommand();
		$models = $command->queryAll();
		$totalItems = $query->count();

		$result = MemberHelper::getSuccessResponse(
			[
				'data' => $models,
				'totalItems' => $totalItems
			]
		);

	  } catch(Exception $e)  {
		$result = MemberHelper::getError($e);
	  }
	  
	  return $this->asJson($result);
	}

	/**
	* Displays a single User model.
	* @param integer $id
	* @return mixed
	*/
	public function actionView(int $id = 0)
	{
		try {
			if (empty($id)) {
				// 參數型態錯誤
				throw new Exception("資料錯誤", 400);
			}

			if (($model = Members::findOne($id)) !== null) {
				$result = MemberHelper::getSuccessResponse([
					'data' => ArrayHelper::toArray($model)
				]);
			}
			if (!$model) {
				throw new Exception("model不存在!!!", 500);
			}
		} catch(Exception $e) {
			$result = MemberHelper::getExceptionErrorResult($e);
		}
	 	return $this->asJson($result);
	}

	/**
	* Creates a new Member model.
	* @return json
	*/
	public function actionCreate()
	{
		try {
			$account = (string) ArrayHelper::getValue($_POST, 'account' , '');
			$password = (string) ArrayHelper::getValue($_POST, 'password' , '');
			$name = (string) ArrayHelper::getValue($_POST, 'name' , '');
			$email = (string) ArrayHelper::getValue($_POST, 'email' , '');

			// 參數型態錯誤
			if ($account=='') {
				throw new Exception("請確認帳號欄位", 400);
			}
			if ($password=='') {
				throw new Exception("請確認密碼欄位", 400);
			}
			if ($name=='') {
				throw new Exception("請確認名稱欄位", 400);
			}
			if ($email=='') {
				throw new Exception("請確認email欄位", 400);
			}

			$member = Members::findbyEmail(
				['email' => $email]
			)->one();
			if (isset($member)) {
				// 判斷是不是同一個人
				throw new Exception("Email已經被註冊!", 400);
			}

			$model = new Members();
			// 前端判斷資料格式是否正確
			$model->account = trim($account);
			$model->password = trim($password);
			$model->name = trim($name);
			$model->email = trim($email);

			$result = MemberHelper::getSuccessResponse([
				'data' => ArrayHelper::toArray($model)
			]);
			// if save fail
			if (!$model->save()) {
				throw new Exception('新增失敗!!', 500);
			}
	  	} catch(Exception $e) {
			$result = MemberHelper::getExceptionErrorResult($e);
		}
		return $this->asJson($result);
	}

	/**
	* Updates an existing User model.
	* @param integer $id
	* @return json
	*/
	public function actionUpdate()
	{
	  try {
		// 轉int
		$id = (int) ArrayHelper::getValue($_POST, 'id' , 0);
		$account = (string) ArrayHelper::getValue($_POST, 'account', '');
		$password = (string) ArrayHelper::getValue($_POST, 'password', '');
		$name = (string) ArrayHelper::getValue($_POST, 'name', '');		
		$email = (string) ArrayHelper::getValue($_POST, 'email', '');

		if (empty($id)) {
			// $id為0，回傳
			// 參數型態錯誤
			throw new Exception("資料錯誤", 400);
		}
		// 參數型態錯誤
		if ($account=='') {
			throw new Exception("請確認帳號欄位", 400);
		}
		if ($password=='') {
			throw new Exception("請確認密碼欄位", 400);
		}
		if ($name=='') {
			throw new Exception("請確認名稱欄位", 400);
		}
		if ($email=='') {
			throw new Exception("請確認email欄位", 400);
		}

		// 判斷有沒有用到其他人設定的email
		$member = Members::findbyEmail(['email'=>$email, 'id'=>$id])->one();
		if (isset($member)) {
			throw new Exception("Email已經被註冊!", 400);
		}

		$model = Members::findIdentity($id);
		if (!$model) {
			throw new Exception("model不存在!", 500);
		}

		$id = (int)$id;
		$account =  trim($account);
		$password = trim($password);
		$name = trim($name);
		$email = trim($email);

		$result = MemberHelper::getSuccessResponse([
			'data' => ArrayHelper::toArray($model)
		]);

		$model->account = $account;
		$model->password = $password;
		$model->name = $name;
		$model->email = $email;

		$result = MemberHelper::getSuccessResponse(
			['data' => ArrayHelper::toArray($model)]
		);
		// if save fail
		if (!$model->save()) {
			throw new Exception('更新失敗', 500);
		}
	  } catch(Exception $e) {
		$result = MemberHelper::getExceptionErrorResult($e);
	  }
	  return $this->asJson($result);
	}

	/**
	* Deletes an existing User model.
	* @param integer $id
	* @return json
	*/
	public function actionDelete(int $id=0)
	{
		try {
			if (empty($id)) {
				// 參數型態錯誤
				throw new Exception("資料錯誤", 400);
			}

			$model = Members::findIdentity($id);
			if (!$model || $model == null) {
				throw new Exception("model不存在!!!", 500);
			}
			
			$result = MemberHelper::getSuccessResponse([
				'data' => ArrayHelper::toArray($model)
			]);
			if (!$model->delete()) {
				// DB錯誤
				throw new Exception("刪除失敗!", 500);
			}
		} catch(Exception $e) {
			$result = MemberHelper::getExceptionErrorResult($e);
		}
	  
	  return $this->asJson($result);
	}
}