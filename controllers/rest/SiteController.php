<?php
namespace app\controllers\rest;

use Yii;

use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\helpers\Security;

use app\models\SignupForm;

class SiteController extends Controller
{
    private $response = [
      'code' => 200,
      'data',
      'message'
    ];  
    public function behaviors()
    {
      return [
          'verbs' => [
            'class' => VerbFilter::className(),
            'actions' => [
                'debug'=>['get'],
                'login'=>['put'],
                'register'=>['post'],
                'logout'=>['put'],
                'forgetpass'=>[''],
                'update'=>['post'],
                'delete'=>['delete']
            ],
          ]
      ];
    }

    public function beforeAction($event)
    {
      $action = $event->id;
      // 判斷是否有設定行為
      if (isset($this->actions[$action])) {
          $verbs = $this->actions[$action];
      } elseif (isset($this->actions['*'])) {
          $verbs = $this->actions['*'];
      } else {
          return $event->isValid;
      }
      // 取得當前的動作
      $verb = Yii::$app->getRequest()->getMethod();
        
      $allowed = array_map('strtoupper', $verbs);
          
        if (!in_array($verb, $allowed)) {
          $this->setHeader(400);
          echo json_encode(array('status'=>0,'error_code'=>400,'message'=>'Method not allowed'),JSON_PRETTY_PRINT);
          exit;
      }  
	
      return true;  
    }

    public function actionDebug()
    {
      $password = 'admin';
      echo $password;
      // security
      echo Yii::$app->security->generatePasswordHash($password);
      // echo Security::generatePasswordHash($password);
    }

    public function actionAddAdmin() {
      $model = User::find()->where(['username' => 'admin'])->one();
      if (empty($model)) {
          $user = new User();
          $user->username = 'admin';
          $user->email = 'admin@devreadwrite.com';
          $user->setPassword('admin');
          $user->generateAuthKey();
          if ($user->save()) {
              echo 'good';
          }
      }
    }

    public function actionRegister()
    {
      $model = new SignupForm();
      $model->attributes = Yii::$app->request->post();
      if ($user = $model->signup()) {
          if (Yii::$app->getUser()->login($user)) {
            $this->response['message']='註冊成功';
          }
      } else {
        $this->response['code'] = '400';
        $this->response['message'] = "註冊失敗";
      }

      // var_dump($model->errors);
      echo json_encode($this->response);
    }
}