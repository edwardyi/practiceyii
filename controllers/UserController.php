<?php
namespace app\controllers;

use Yii;

use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
* UserController implements the CRUD actions for User model.
*/
class UserController extends Controller
{
   /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
      return [
          'access' => [
              'class' => AccessControl::className(),
              'only' => ['logout'],
              'rules' => [
                  [
                      'actions' => ['logout'],
                      'allow' => true,
                      'roles' => ['@'],
                  ],
              ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'logout' => ['post'],
              ],
          ],
      ];
  }

  public function actionIndex()
  {
    $title = "會員列表";
    return $this->render('index', ['title'=>$title]);
  }

  public function actionCreate()
  {
    $title = "新增會員";
    
    return $this->render('create', ['title'=>$title]);
  }

  public function actionUpdate($id)
  {
    $title = "修改會員";
    
    return $this->render('update', ['title'=>$title]);
  }
}