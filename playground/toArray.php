<?php

echo 'Hello World!';

class ObjectA{
	public static $name = "test";
	protected $cname = "ObjectA";
	public function getName() {
			 return $name;
	}
}
$obj = new ObjectA();
$aa = toArray($obj, ["ObjectA"=>["name"=>"hello"]]); 
var_dump($aa);

function toArray($object, $properties = [], $recursive = true)
{
	if (is_array($object)) {
		if ($recursive) {
				foreach ($object as $key => $value) {
						if (is_array($value) || is_object($value)) {
								$object[$key] = toArray($value, $properties, true);
						}
				}
		}

		return $object;
		} elseif (is_object($object)) {
			if (!empty($properties)) {
				$className = get_class($object);
				var_dump($properties,$className);
				if (!empty($properties[$className])) {
							$result = [];
							foreach ($properties[$className] as $key => $name) {
									if (is_int($key)) {
											$result[$name] = $object->$name;
									} else {
											$result[$key] = getValue($object, $name);
									}
							}

							return $recursive ? toArray($result, $properties) : $result;
					}
			}
			if ($object instanceof Arrayable) {
					$result = $object->toArray([], [], $recursive);
			} else {
					$result = [];
					foreach ($object as $key => $value) {
							$result[$key] = $value;
					}
			}

		return $recursive ? toArray($result, $properties) : $result;
	}
	return [$object];
}

function getValue($array, $key, $default = null)
{
	if ($key instanceof \Closure) {
		return $key($array, $default);
	}

	if (is_array($key)) {
		$lastKey = array_pop($key);
		foreach ($key as $keyPart) {
			$array = getValue($array, $keyPart);
		}
		$key = $lastKey;
	}

	if (is_array($array) && (isset($array[$key]) || array_key_exists($key, $array))) {
		return $array[$key];
	}

	if (($pos = strrpos($key, '.')) !== false) {
		$array = getValue($array, substr($key, 0, $pos), $default);
		$key = substr($key, $pos + 1);
	}

	if (is_object($array)) {
		// this is expected to fail if the property does not exist, or __get() is not implemented
		// it is not reliably possible to check whether a property is accessible beforehand
		return $array->$key;
	} elseif (is_array($array)) {
		return (isset($array[$key]) || array_key_exists($key, $array)) ? $array[$key] : $default;
	}

	return $default;
}