<?php
namespace app\assets;

use yii\web\AssetBundle;

class BaseAsset extends AssetBundle
{
  // 指向網站入口位置
  public $sourcePath = '@webroot';
  // 一般BaseAsset不定義自己的js和css屬性，由子類別個別定義
  public $js = [
    'js/common.js',
    'https://code.jquery.com/jquery-3.3.1.min.js'
  ];
}