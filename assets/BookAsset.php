<?php
namespace app\assets;

use yii\web\AssetBundle;

class BookAsset extends AssetBundle
{
    public $sourcePath = '@app/assets';
    // 引用Vue，取得namespace
    public $depends = [
        'app\assets\VueAsset',
    ];
}
