<?php
namespace app\assets;

use yii\web\AssetBundle;
// use app\assets\edward\BaseAsset;

// https://code.tutsplus.com/tutorials/how-to-program-with-yii2-working-with-asset-bundles--cms-23226 
class VueAsset extends BaseAsset
{
    // public $sourcePath = '@asset';
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    // 繼承父類別共用資源
    public function init() {
      parent::init();
      $this->js[] = 'https://cdn.jsdelivr.net/npm/vue@2.6.8/dist/vue.js';
      $this->js[] = 'js/vuebook.js';
    }
}  