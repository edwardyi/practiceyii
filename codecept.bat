@echo off

rem -------------------------------------------------------------
rem  Composer command line for Windows.
rem
rem -------------------------------------------------------------

@setlocal


rem cd /d e:\laragon\bin\composer\
if "%COMPOSER_COMMAND%" == "" set COMPOSER_COMMAND="cd e:\laragon\bin\composer\composer.exe"


"%COMPOSER_COMMAND%" "exec -v codecept" %*

@endlocal

