<?php
namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;

class HelloWidget extends Widget{
    public $message;

    // 要放屬性
    public function init(){
        parent::init();
        if($this->message===null){
            $this->message= 'Welcome User';
        }else{
            $this->message= 'Welcome '.$this->message;
        }
    }

    // 要放輸出
    public function run(){
        return Html::encode($this->message);
    }
}
?>