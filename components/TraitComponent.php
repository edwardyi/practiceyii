<?php

namespace app\components;

trait Mouse {
    public $name = '鼠标';
    public function click() {
        echo "鼠标点击了一下";
    }
}

class Computer {
    public function sayName(){
        echo "我是一台电脑";
    }
}

class Macbook extends Computer {
    use Mouse;
    public function say(){
        echo "我是一条有逼格的macbook";
    }
}

$mac = new Macbook();
$mac->say();
$mac->click();