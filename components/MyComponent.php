<?php
namespace app\components;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
 
class MyComponent extends Component
{
  
  public $setting;
  
  public function welcome()
  {
    echo $this->setting." Hello..Welcome to MyComponent";
  }
 
}