<?php
namespace app\components;


use yii\web\UploadedFile;
use yii\base\Behavior;
use yii\db\BaseActiveRecord;

class AttachmentsBehavior extends Behavior {

  private $_files;
  /**
   * 需要上传的文件属性
   * @var string
   */
  public $uploadFiles = 'uploadfiles';
  /**
   * 已经上传了的文件属性
   * @var string
   */
  public $uploadedFiles = 'uploadedfiles';

  /**
   * 保存路径
   * @var string
   */
  public $savePath = '@common/upload';

  /**
   * 访问路径
   * @var string
   */
  public $saveUrl = '@commonurl/upload';

  public function events() {
      return [
          BaseActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
          BaseActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
          BaseActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
          BaseActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
      ];
  }

  /**
   * This method is invoked before validation starts.
   */
  public function beforeValidate()
  {
     $this->_files = UploadedFile::getInstances($this->owner, $this->uploadFiles);
  }

  /**
   * 返回拥有者的唯一Id
   * @return string
   */
  public function getIdentityId(){
      return  $this->owner->className().'.'.$this->owner->id;
  }

  /**
   * 明确拥有者与附件的关系
   * @return mixed
   */
  public function getAttachments(){
      return $this->owner->hasMany(Attachments::className(),['ownerId' => 'identityId']);
  }

  /**
   * 在主模型保存后挨个保存附件
   */
  public function afterSave(){

      foreach ($this->_files as $file){
          $model = new Attachments();
          $model->fileName = $file->name;
          $model->url = date('Ymd') . Yii::$app->getSecurity()->generateRandomString(8) .'.'. $file->extension;
          $model->ownerId = $this->owner->identityId;
          $model->savePath = Yii::getAlias($this->savePath);
          $file->saveAs(Yii::getAlias($this->savePath) . DIRECTORY_SEPARATOR .$model->url);
          $model->save();
      }

  }

  /**
   * 在主模型删除之前删除所有附件
   * @return bool
   */
  public function beforeDelete(){

      foreach ($this->owner->{$this->uploadedFiles} as $file){
          $file->delete();
      }
      return true;
  }


  public function getFilesUrl($url){
      return Yii::getAlias($this->saveUrl) . DIRECTORY_SEPARATOR. $url;
  }

}