<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user}}`.
 */
class m190319_020058_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string(255)->notNull(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string(255)->notNull(),
            'password_reset_token' => $this->string(255),
            'email' => $this->string(255)->notNull(),
            'role' => $this->string(64)->notNull(),
            'status' => $this->integer(6)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
        ]);

        $this->insert('user', [
            'username' => 'admin',
            'auth_key' => 'usDE5VMsMk-pIavWcODwuCxo7t86Ofq5',
            'password_hash' => '$2y$13$QbtABt56PQmjVyyKFsz94eoWeE1fwF0zMZXRmu5odKp7GzE0QRgMm',
            'password_reset_token' => 'Q8saPCmKheitPY6ytN9wouG5hRGQ_WQr_1458090182',
            'email' => 'admin@demo.com',
            'role' => 'admin',
            'status' => '1',
            'created_at' => 1458090182,
            'updated_at' => 1458090182,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
