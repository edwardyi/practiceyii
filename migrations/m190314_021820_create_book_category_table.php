<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%book_category}}`.
 */
class m190314_021820_create_book_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // $this->safeDown();
        $this->createTable('{{%book_category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'create_date' => $this->dateTime() //. ' DEFAULT CURRENT_TIMESTAMP ',
        ]);

        $this->insert('book_category', [
            'name' => '未分類'
        ]);
        $this->insert('book_category', [
            'name' => '小說'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%book_category}}');
    }
}
