<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Handles the creation of table `{{%blog}}`.
 */
class m190313_105033_create_blog_table extends Migration
{

    public function up()
    {
        $this->createTable('blogs', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'content' => Schema::TYPE_TEXT,
            'remark' => Schema::TYPE_TEXT,
        ]);
    }

    public function down()
    {
        $this->dropTable('blogs');
    }
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%blog}}', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%blog}}');
    }
}
