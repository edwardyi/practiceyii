<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%attachments}}`.
 */
class m190314_085023_create_attachments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%attachments}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'model' => $this->string(255)->notNull(),
            'itemId' => $this->integer()->notNull(),
            'hash' => $this->string(255)->notNull(),
            'size' => $this->integer()->notNull(),
            'type' => $this->string(255)->notNull(),
            'mime' => $this->string(255)->notNull(),
            'identityId' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%attachments}}');
    }
}
