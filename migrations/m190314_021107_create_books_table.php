<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%books}}`.
 */
class m190314_021107_create_books_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // $this->safeDown();
        $this->createTable('{{%books}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'borrowDate' => $this->datetime()->notNull(),
            'email' => $this->string(255),
            'name' => $this->string(255)->notNull(),
            'price' => $this->integer(11),
            'cate_id' => $this->integer(11).' default 1',
            'created_at' => $this->datetime(), //. ' DEFAULT CURRENT_TIMESTAMP ',
            'updated_at' => $this->datetime(), //. ' DEFAULT CURRENT_TIMESTAMP ',
        ]);

        $this->insert('books', [
            'title' => '神鵰俠侶',
            'borrowDate' => '2019-03-14 09:33:00',
            'email' => 'test@qq.com.tw',
            'name' => '楊過',
            'price' => 500,
            'cate_id' => 1,
        ]);
        $this->insert('books', [
            'title' => '鹿鼎記',
            'borrowDate' => '2019-03-14 09:50:00',
            'email' => 'test2@qq.com.tw',
            'name' => '尾小寶',
            'price' => 300,
            'cate_id' => 1,
        ]);
        $this->insert('books', [
            'title' => '笑傲江湖',
            'borrowDate' => '2019-03-14 09:50:00',
            'email' => 'test3@qq.com.tw',
            'name' => 'Jason',
            'price' => 500
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%books}}');
    }
}
