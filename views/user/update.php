<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

use app\assets\MemberAsset;

// 引用jquery
MemberAsset::register($this);
$this->title = $title;
?>
<div class="site-contact">
    <br/><br/>
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-lg-12">
            <form  method="post" id="memberForm">
                <label>名稱</label>
                <input class="form-control" type="text" id="name" name="name" value="" /> <br/>
                <label>帳號:</label>
                <input class="form-control" type="text" id="account" name="account" value="" /> <br/>
                <label>密碼:</label>
                <input class="form-control" type="text" id="password" name="password" value="" /> <br/>
                <label>Email:</label>
                <input class="form-control" type="text" id="email" name="email" value="" /> <br/>
                <input class="form-control" type="hidden" id="id" name="id" value="" /> <br/>
                <input type="button" class="btn btn-primary" value="修改" onclick="updateMemeber()"/>
            </form>
        </div>
    </div>
</div>
