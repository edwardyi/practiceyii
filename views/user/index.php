<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use app\assets\MemberAsset;

// 引用jquery
MemberAsset::register($this);
$this->title = $title;
?>
<div class="site-index">

    <div class="body-content">
        <div class="row">
            <div class="col-lg-12">
            <br/><br/>
                <h2><?php echo $title;?></h2>
                <a href="create" class="btn btn-primary">新增</a>
                <table id="member" class="table .table-striped">
                  <thead>
                    <tr>
                      <td>帳號</td>
                      <td>姓名</td>
                      <td>Email</td>
                      <td>建立日期</td>
                      <td>更新日期</td>
                      <td>操作</td>
                    </tr>
                  </thead>
              </table>
            </div>
          
        </div>

    </div>
</div>
