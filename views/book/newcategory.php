<?php 

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(['id' => 'category-form']); ?>

    <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('更新', ['class' => 'btn btn-primary', 'name' => 'borrow-button']) ?>
    </div>

<?php ActiveForm::end(); ?>