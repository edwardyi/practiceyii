<?php 

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(['id' => 'borrow-form']); ?>

    <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
    <?= $form->field($model, 'title') ?>
    <?= $form->field($model, 'borrowDate') ?>
    <!-- 新增input type="file" 類型會增加 multipart/form-data的屬性 -->
    <?= $form->field($model, 'imageFile')->fileInput() ?>
    <div class="form-group">
        <?= Html::submitButton('更新', ['class' => 'btn btn-primary', 'name' => 'borrow-button']) ?>
    </div>

<?php ActiveForm::end(); ?>