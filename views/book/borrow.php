<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

// use app\assets\BaseAsset;
use app\assets\BookAsset;

// 註冊Vue的assets(css和js一併引入)
BookAsset::register($this);

$this->title = '借書';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <div id="app">{{content}}</div>
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>

        </div>
    </div>
</div>
