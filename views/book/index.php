<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

use app\components\HelloWidget;

$this->title = 'Books';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="books-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php echo Yii::$app->mycomponent->welcome();?>
    <br/>
    <?php echo HelloWidget::widget(
        ['message'=>'客製化Widget']
    ) ?>
    <p>
        <a href="<?php echo Url::to('index.php?r=book/borrow');?>" class="btn btn-success" >借書<a>
        <a href="<?php echo Url::to('index.php?r=book/NewCategory');?>" class="btn btn-primary" >建立分類<a>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $book,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'title',
            'categoryName',
            'customed_name',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>

