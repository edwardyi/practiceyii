<?php
namespace app\models\Category;
use Yii;
/**
 * This is the model class for table "customer".
 *
 * @property integer $id_customer
 * @property string $name
 * @property string $email
 * @property string $city
 * @property string $country
 */
class BookCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'books_category';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          [['name'], 'required']
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
          'name' => '分類名稱',
          'create_date' => '建立日期'
        ];
    }
}