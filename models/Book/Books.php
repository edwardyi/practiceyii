<?php
namespace app\models\Book;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use app\models\Book\BookCategory;

use app\components\AttachmentsBehavior;
use app\components\ImageBehavior;
/**
 */
class Books extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'books';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          [['name', 'title', 'borrowDate'], 'required'],
          ['email', 'email']
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
          'name' => '借閱人',
          'email' => '電子郵件',
          'title' => '書名',
          'borrowDate' => '借閱日期',
          'cate_id' => '分類',
          'categoryName' => '分類',
          'customed_name' => '測試'
        ];
    }

    public function getBookCategories()
    {
      // https://www.yiiframework.com/doc/guide/2.0/en/db-active-record#declaring-relations
      return $this->hasOne(BookCategory::className(), ['id' => 'cate_id']);
        // return $this->hasMany(self::className(), ['cate_id' => 'id']);
    }

    /* Getter for country name */
    public function getCategoryName()
    {
      // var_dump($this->bookCategories);exit;
      return $this->bookCategories->name;
    }
    public function getCustomed_Name()
    {
      return '測試中...';
    }

    public function behaviors()
    {
        return [
          'timestamp' => [
              'class' => 'yii\behaviors\TimestampBehavior',
              'attributes' => [
                  ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                  ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
              ],
              'value' => new Expression('NOW()'),
          ],
          [
              'class' => AttachmentsBehavior::className(),
              'uploadFiles' => 'attachmentsToBe',
              'uploadedFiles' => 'attachments',
          ],
            'ImageBehavior' => [
              'class' => ImageBehavior::className(),
              'imageModel' => 'models\Image',
              'imageSizeModel' => 'models\ImageSize',
              'imageVariable' => 'file',
              'imageFolder' => '@upload',
              'webImageFolder' => '@webupload',
              'noImagePath' => '@webupload/no-image.png',
          ],
      ];
    }

}