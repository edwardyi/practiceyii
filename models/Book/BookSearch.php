<?php
namespace app\models\Book;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Book\Books;
/**
 * BookSearch represents the model behind the search form about `app\models\Customer`.
 */
class BookSearch extends Books
{
    public $cate_name;
    // private $cate_name;
    public function rules()
    {
        // 查詢的時候不需要卡必填欄位
        return [
          [['name', 'title', 'borrowDate','cate_id'], 'safe'],
          ['email', 'email']
        ];
    }
  
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Books::find();
        // join with加上關聯(開頭只能小寫)
        // $query->select("books.*, book_category.name as cate_name1")->joinWith(['bookCategories']);
        // ->orderBy("cate_name DESC");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10
            ]
        ]);
        
        // var_dump($dataProvider->query->createCommand()->rawSql);exit;
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'cate_id', $this->cate_id]);
            // ->andFilterWhere(['like', 'cate_name', $this->cate_name]);
        return $dataProvider;
    }


    public function searchWith($params)
    {
        // with:用sql in
        // joinwith:預設用left join
        $query = Books::find()->with('books qq');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10
            ]
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'email', $this->email]);
        return $dataProvider;
    }
}