<?php
namespace app\models\Book;

use Yii;
use yii\base\Model;

class BookForm extends Books
{
  public $title;
  public $name;
  public $email;
  public $price;
  public $borrowDate;

  public function borrow()
  {
    if($this->validate()){
      return true;
    }
    return false;
  }
}