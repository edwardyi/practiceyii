<?php
namespace app\models\Book;
use Yii;

/**
 */
class BookCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'book_category';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          [['name'], 'required']
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
          'name' => '分類',
          'create_date' => '建立日期'
        ];
    }
}