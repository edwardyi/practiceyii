<?php
namespace app\helpers;

class MemberHelper {
    private static $response = [
		'code' => 200,
		'message' => '取得資料成功'
    ];

    public static function getErrorStatusDescription()
    {
        return [
            300 => '資源重導', // 交給別人處理
            400 => '資源不存在',
            500 => '伺服器內部錯誤'
        ];
    }

    public static function getSuccessResponse($extraData = [])
    {
        return array_merge(self::$response, $extraData);
    }

    public static function getErrorResponse($data)
    {
        // $code=200 , $message=""
        $code = isset($data['code']) ? (int) $data['code'] : 999;
        $message = isset($data['message'])  ? (string) $data['message'] : '未知錯誤';

        return self::$response = $code == 200 ? self::$response : [
            'code' => $code,
            'message' => empty($message) ? self::getErrorStatusDescription()[$code]: $message
        ];
    }

    public static function getResponse()
    {
        return $response;
    }

    public static function getError($e)
	{
		// 回傳錯誤格式
		return [
			'code' => $e->getCode(),
			'message' => $e->getMessage()
		];
	}
    
    // Exception回傳的值
    public static function getExceptionErrorResult($e)
    {
        $error = self::getError($e);
		return self::getErrorResponse($error);
    }
}